# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_count = Hash.new(0)
  str.split.each do |word|
    word_count[word] = word.length
  end

  word_count
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v}.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    if older.has_key?(key)
      older[key] = value
    else
      older[key] = value
    end
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count = Hash.new(0)
  word.chars.each do |letter|
    letter_count[letter] += 1
  end

  letter_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each do |el|
    hash[el] = 0
  end

  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = Hash.new(0)
  numbers.each do |number|
    if number.even?
      hash[:even] += 1
    else
      hash[:odd] += 1
    end
  end

  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  common_vowels = Hash.new(0)
  only_vowels = string.chars.select { |letter| "aeiou".include?(letter) }
  only_vowels.each do |vowel|
    common_vowels[vowel] += 1
  end

  largest_value = common_vowels.values.max
  ties = common_vowels.select { |k, v| v == largest_value }
  ties.min[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  later_birth_months = students.select { |k, v| v > 6 }

  groups = []
  later_birth_months.each_key do |key|
    keys = later_birth_months.keys
    keys.delete(key)
    groups << keys
  end

  groups
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  population = Hash.new(0)
  number_of_species = specimens.uniq.length
  specimens.each do |animal|
    population[animal] += 1
  end
  smallest_population_size = population.values.min
  largest_population_size = population.values.max
  biodiversity = {}
  biodiversity[population.keys] = number_of_species**2 *
   smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  # get letter count of the normal sign
  # subtract the values of the letters of the vandalized_sign from the normal sign
  vandal_letters = character_count(vandalized_sign)
  normal_letters = character_count(normal_sign)
  normal_letters.each do |letter, value|
    if vandal_letters.has_key?(letter)
      normal_letters[letter] -= vandal_letters[letter]
    end
  end

  normal_letters.values.min >= 0
end

def character_count(str)
  letter_count = Hash.new(0)
  str.each_char do |letter|
    letter_count[letter] += 1
  end

  letter_count
end
